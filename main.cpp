#include "mbed.h"
#include "DS1820.h"
#include <string>
#include <sstream>

/**
 * PA_9 - TX pin (RX on the GSM side)
 * PA_10 - RX pin (TX on the WiFi side)
 * 4800 - Baud rate
 */

//for SIM800L
Serial SIM800L(PA_9, PA_10);
//for Desktop terminal
Serial pc(USBTX, USBRX);
//for Temperature module
DS1820 ds1820(PA_8);

DigitalOut rele(D5);

char x;

//read 
void callback_rx()
{
    while (SIM800L.readable())
    {
        x = SIM800L.getc();
        pc.putc(x); // print the answer from SIM800
    }
}


typedef enum bad_temp
{
    low,
    hight
}           badTemp;

void    sendMessage(float cur_temp, badTemp sign)
{
    SIM800L.printf("AT+CMGF=1");//at command for send sms
    SIM800L.putc(0x0d);
    wait(1);   
    SIM800L.printf("AT+");
    wait(1);
    SIM800L.printf("CMGS=");
    SIM800L.putc('"');
    SIM800L.printf("+380938143061"); 
    SIM800L.putc('"');
    SIM800L.putc(0x0d);
    wait(1);
    if (sign == low)
        SIM800L.printf("Alert Temperature is too low %.1f", cur_temp);
    else
        SIM800L.printf("Alert: Temperature is too high %.1f", cur_temp);
    SIM800L.putc(0x0d);
    SIM800L.putc(0x1a);
    wait(10);
}


//#define DEB

int main()
{
    // Initialize the interface.
    // If no param is passed to init() then DHCP will be used on connect()
    SIM800L.attach(&callback_rx);  
    SIM800L.baud(4800);
 
 #ifdef DEB
     while(1) {
        if(pc.readable()) {
            eth.putc(pc.getc());
        }
    }
 
    wait(1);
 #endif
 
    while (42)
    {
        pc.printf("Started\n");
        float cur_temp;
        while (1){
            if (ds1820.begin()){
                ds1820.startConversion();
                cur_temp = ds1820.read();
                pc.printf("%f\n", cur_temp);
                //server part here
                //end of server part
                if (cur_temp > 26.0 || cur_temp < 20.0)
                {
                    char s[50];
                    sprintf (s, "{\"temperature\": %.1f }", cur_temp);
                    wait(2);
                    sendMessage(cur_temp, cur_temp > 27.0 ? hight : low);
                    //send signal to rele
                    //pc.printf("nochator\n");
                    rele.write(1);
                    wait(1);
                    rele.write(0);
                    wait(15);
                }
                wait(1);
            }
        }        
    }
    return 0;
}